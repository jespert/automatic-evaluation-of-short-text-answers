# Automatic evaluation of short text answers

This is the repository for the Master thesis of Science in Informatics with the title Automatic Evaluation of Short Text Answers with Feedback Techniques to Enhance Student Learning Performance. The thesis is written by Jesper Trøan and Julian Nyland Skattum.

