import numpy as np
import pandas as pd
import ast
import matplotlib.pyplot as plt
import statsmodels.api as sm
from scipy.stats import kurtosis, shapiro




def main():
    df3 = pd.read_csv('Filer\IT2810Host2018Processed.csv')
    extract_features(df3)
    plot_grades(df3)
    plot_answer_lengths(df3)
    answer_features_to_csv(df3)


def extract_features(df):
    """
    Function that extractes the features of the datasets.
    
    Args:
    df (dataframe): The dataframe to extract from.
    """
    #Count the answers for each grade
    value_counts = df['Manualscore'].value_counts()
    print("Grades:", value_counts)

    mean = df['Manualscore'].mean()
    print("Mean grade:", mean)

    median = df['Manualscore'].median()
    print("Median grade:", median)

    std = df['Manualscore'].std()
    print("std grade:", std)

    skewness = 3 * (mean - median) / std

    print("Skewness:", skewness)

    k = kurtosis(value_counts)
    print("kurtosis:", k)

    unique_count = df['candidateId'].nunique()
    print("Number of students participating:", unique_count)

    # Convert column object str back to list. Necessary when reading from csv
    df['tokens'] = df['tokens'].apply(ast.literal_eval)

    df['A_length'] = df['tokens'].apply(lambda x: len(x))

    count_list = df['tokens'].apply(lambda x: len(x))

    print("Mean word count:", count_list.mean())

    print("Median word count:", count_list.median())

    print("Min word count:", count_list.min())
    
    print("Max word count:", count_list.max())

    print("std word count:", count_list.std())
    
def plot_grades(df):
    """
    Function that plots the number of answers for each grade.
    
    Args:
    df (dataframe): The dataframe to extract from.
    """
    value_counts = df['Manualscore'].value_counts()
    # Create the bar chart
    plt.bar(value_counts.index, value_counts.values, width=0.7)

    # Add labels and title
    plt.xlabel('Grades')
    plt.ylabel('Counts')
    plt.title('Grade Distribution E19')


    # Show the chart
    plt.show()

def plot_answer_lengths(df):
    """
    Function that plots a histogram of the answer lengths for each grade
    
    Args:
    df (dataframe): The dataframe to extract from.
    """
    # Convert column object str back to list. Necessary when reading from csv
    #df['tokens'] = df['tokens'].apply(ast.literal_eval)
    count_list = df['tokens'].apply(lambda x: len(x))

    # Read the data into a DataFrame
    # Create the X and Y variables
    X = count_list
    Y = df['Manualscore']

    # Add a constant to the X variable
    #X = sm.add_constant(X)

    #Loop through all grades
    for i in range(6):
        print("for grade", i, ":")
        df_grade = df.query("Manualscore == @i")
        num_answers = df_grade.shape[0]
        print("number of answers:", num_answers)
        count_list = df_grade['tokens'].apply(lambda x: len(x))        
        print("Mean word count:", count_list.mean())
        mean = count_list.mean()
        std = count_list.std()
        print("Median word count:", count_list.median())
        print("Min word count:", count_list.min())
        print("Max word count:", count_list.max())
        print("std word count:", count_list.std())

        # plot the histogram
        ax = count_list.plot.hist(bins=20, alpha=0.6, rwidth=0.85)
        ax.axvline(mean, color='g', linestyle='dashed', linewidth=2)
        ax.axvline(mean - std, color='r', linestyle='dotted', linewidth=2)
        ax.axvline(mean + std, color='r', linestyle='dotted', linewidth=2)
        # add mean and std text to the plot, set the y value to middle of the y axis
        plt.text(mean+2*std, plt.ylim()[1]/2, 'Mean: {:.2f}\nStd: {:.2f}'.format(mean, std),
                bbox=dict(facecolor='white', alpha=0.5))

        # set title and labels
        plt.title('Histogram of Responses')
        plt.xlabel('Word Count')
        plt.ylabel('Frequency')

        plt.show()

def answer_features_to_csv(df):
    """
    Function that creates a CSV file with the answer_length features
    
    Args:
    df (dataframe): The dataframe to extract from.
    """
    feature_df = pd.DataFrame(columns=['Manualscore', 'mean_count', 'std_count'])

    for i in range(6):
        df_grade = df.query("Manualscore == @i")
        count_list = df_grade['tokens'].apply(lambda x: len(x))
        mean = count_list.mean()
        std = count_list.std()
        feature_df.loc[i] = [i, mean, std]

    feature_df.to_csv("answer_length_features_2018.csv")



   # Run main
if __name__ == "__main__":
    main()