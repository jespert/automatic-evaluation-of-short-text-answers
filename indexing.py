from threading import main_thread
import numpy as np
import pandas as pd
import json
import html
import ast
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem.snowball import SnowballStemmer
import string
import re
import sys
import html
from polyglot.detect import Detector
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import glob
from sklearn.decomposition import TruncatedSVD


def main():
    #Choose year and column of the dataframe to make matrix for
    make_term_document_matrix('2018', 'lemmatized')
    # run_algorithm()
    similarity_against_common_terms('2018')
    # similarity_against_suggested_solution_terms()
    # similarity_against_common_terms('2019')

def make_term_document_matrix(year: string, column_name: string):
    """
    Function that create a separate term document matrix for each question. These matrixes are saved in the
    "201*_tdm_per_question folders.
    
    Args:
    year (string): The year of the dataframe.
    column_name: The column name to make matrixes for
    """
    df3 = pd.read_csv('Filer\IT2810Host'+year+'Processed.csv')
    unique_qIds = df3.questionId.unique()

    for id in unique_qIds:
        df = df3.query("questionId == @id")
    # join the list of strings
    # df['stemmed'] = df['stemmed'].apply(lambda x: ast.literal_eval(x))
    # df["joined"] = df["stemmed"].apply(lambda x: " ".join(x))

        # Ignore terms that appear in less than 10% of the documents
        vectorizer = TfidfVectorizer()

        # Fit the vectorizer on the column synonyms
        X = vectorizer.fit_transform(df[column_name])

        count_array = X.toarray()

        # convert the Series of 'candidateId' and 'questionId' columns to a list of strings to use as intexes
        index = ["Candidate: " + str(x) + " questionId: " + str(y)
                 for x, y in zip(df['candidateId'].tolist(), df['questionId'].tolist())]

        # Make dataframe with index candidateId and questionId
        tdm = pd.DataFrame(count_array, columns=vectorizer.get_feature_names_out())

        # Insert columns for question id, candidate id and manualscore
        tdm.insert(0, "questionId", [x for x in df["questionId"].tolist()], True)
        tdm.insert(0, "candidateId", [x for x in df["candidateId"].tolist()], True)
        tdm.insert(2, "Manualscore", [x for x in df["Manualscore"].tolist()], True)

        question_tdm = tdm.loc[:, (tdm != 0).any(axis=0)]

        # Write each matrix to file
        question_tdm.to_csv("./"+year+"_tdm_per_question/" + str(id) + ".csv", index=False)
        print("written " + str(id) + " to file")


def extract_common_terms(df_high_grades, n):
    """
    Function that extractes the N most common terms from high grades
    
    Args:
    df_high_grades (dataframe): A dataframe that only contains the highly grades answers
    n (int): How many terms to extract
    """
    # Sum up columns in the dataframe
    sum_of_columns = df_high_grades.sum(axis=0)
    # Return the top n terms in a list
    return sum_of_columns.sort_values(ascending=False).head(n).index.tolist()



def get_feedback_terms(high_grade_terms, candidate_terms):
    """
    Function that extractes possible feedback terms.
    
    Args:
    high_grade_terms (list): A list of high-grade terms.
    candidate_terms (list): A list of candidate terms.
    """
    feedback_terms = []
    for term in high_grade_terms:
        if term not in candidate_terms:
            feedback_terms.append(term)
    return feedback_terms


def cosine_similarity_to_grade(similarity_score):
    """
    Transforms a cosine similarity score to a grade on a scale of 0-5.
    """

    # All similarities over the threshold will be assigned the grade in the same tupple as the threshold
    intervals = [
        (0.3, 5),
        (0.25, 4),
        (0.2, 3),
        (0.1, 2),
        (0.05, 1),
        (0.0, 0)
    ]
    # Loop through the intervals, find the grade that corresponds to that threshold
    for threshold, grade in intervals:
        if similarity_score >= threshold:
            return grade

    # If the similarity score is negative, return 0; otherwise, return the minimum grade (0)
    return 0 if similarity_score < 0 else intervals[-1][1]


def get_length_multiplier(length, df):
    """Calculate a multiplier based on the length of a response and a dataframe of score means.

    Args:
        length (int): The length of the response to be graded.
        df (pandas.DataFrame): A dataframe with columns 'Manualscore', 'mean_count', and 'std_count',
            where each row represents a score level and the mean and standard deviation of the response lengths
            associated with that score level.
    Returns:
        float: A multiplier to be applied to the grade of the response.
    """
    if length >= df.loc[df['Manualscore'] == 5, 'mean_count'].values[0]:
        return 1.8
    elif length >= df.loc[df['Manualscore'] == 4, 'mean_count'].values[0]:
        return 1.6
    elif length >= df.loc[df['Manualscore'] == 3, 'mean_count'].values[0]:
        return 1.4
    elif length >= df.loc[df['Manualscore'] == 2, 'mean_count'].values[0]:
        return 1.2
    elif length >= df.loc[df['Manualscore'] == 1, 'mean_count'].values[0]:
        return 1.1
    elif length >= df.loc[df['Manualscore'] == 0, 'mean_count'].values[0]:
        return 1.05
    elif length < df.loc[df['Manualscore'] == 0, 'mean_count'].values[0]:
        return 1


# Reference answer test 1: All highest graded answers
def most_common_terms_allHighestGradedAnswers(year: string):

    path = year + '_tdm_per_question/*.csv'
    correlations = []
    for filename in glob.glob(path):
        df = pd.read_csv(filename)

        similarity_df = pd.DataFrame(columns=['AVG_cos', 'Manualscore'])

        # Instantiate the TruncatedSVD object
        #svd = TruncatedSVD(n_components=100)
        threshold = 0.02

        # Fit the SVD object to the term document matrix
        # svd.fit(df)

        # Create a mask with the same shape as the term document matrix
        mask = np.zeros(df.shape)

        # Set the elements in the mask that correspond to the TF-IDF weights that are greater than or equal to the threshold to 1
        mask[df >= threshold] = 1

        # Multiply the term document matrix with the mask to set the TF-IDF weights under the threshold to 0
        tdm_filtered = df * mask

        # Sort by manualscore
        df_high_grades_sorted = tdm_filtered.sort_values(
            by='Manualscore', ascending=False)

        # Get number of high grade answers
        question_tdm = tdm_filtered.query("Manualscore == 5")
        num_high_grades = len(question_tdm.index)

        # Use the first 10 high grade answers as reference answers
        df_high_grades = df_high_grades_sorted.head(int(num_high_grades))

        # Remove not relevant attributes for the cosine similarity calculation
        df_high_grades = df_high_grades.drop(
            ['candidateId', 'questionId', 'Manualscore'], axis=1)

        # Get the most common terms from the high grades
        most_common_terms = extract_common_terms(
            df_high_grades, 20)

        # Use all under the first 10 rows as Student answers
        df_rest_of_responses = df_high_grades_sorted.iloc[int(
            num_high_grades):]

        for i in range(len(df_rest_of_responses)):

            test = df_rest_of_responses.iloc[[i]]
            manualScore = test.iloc[0]['Manualscore']

            # Remove not relevant attributes for the cosine similarity calculation
            # print(test.candidateId)
            test = test.drop(
                ['candidateId', 'questionId', 'Manualscore'], axis=1)

            # get feedback
            #candidate = test.loc[:, (test != 0).any(axis=0)]
            #candidate_term_list = list(candidate)
            # print(candidate_term_list)
            # feedback_terms = get_feedback_terms(
            #    most_common_terms, candidate_term_list)

            #print("terms for feedback of this candidate " + str(feedback_terms))

            # Calculate the cosine similarity between the response and all other high grades responses
            similarities = cosine_similarity(
                test.values.reshape(1, -1), df_high_grades.values)

            # Calculate the average cosine similarity between one anwer and all high grades responses
            similarities = similarities[0]

            average_similarity = sum(similarities) / len(similarities)

            # Append the manualscore and average cosine similarity to the similarity df
            similarity_df = similarity_df.append(
                {'AVG_cos': average_similarity, 'Manualscore': manualScore}, ignore_index=True)

        # Calculate the pearlson correlation
        correlation = similarity_df['AVG_cos'].corr(
            similarity_df['Manualscore'])
        correlations.append(correlation)
    print(correlations)
    avg_corr = sum(correlations) / len(correlations)
    print(avg_corr)


# Reference answer test 2: ten highest graded answers as reference answers
def most_common_terms_tenHighestGradedAnswers(year: string):

    path = year + '_tdm_per_question/*.csv'
    correlations = []
    for filename in glob.glob(path):
        df = pd.read_csv(filename)

        similarity_df = pd.DataFrame(columns=['AVG_cos', 'Manualscore'])

        # Instantiate the TruncatedSVD object
        #svd = TruncatedSVD(n_components=100)
        threshold = 0.02

        # Fit the SVD object to the term document matrix
        # svd.fit(df)

        # Create a mask with the same shape as the term document matrix
        mask = np.zeros(df.shape)

        # Set the elements in the mask that correspond to the TF-IDF weights that are greater than or equal to the threshold to 1
        mask[df >= threshold] = 1

        # Multiply the term document matrix with the mask to set the TF-IDF weights under the threshold to 0
        tdm_filtered = df * mask

        # Sort by manualscore
        df_high_grades_sorted = tdm_filtered.sort_values(
            by='Manualscore', ascending=False)

        # Use the first 10 high grade answers as reference answers
        df_high_grades = df_high_grades_sorted.head(10)

        # Remove not relevant attributes for the cosine similarity calculation
        df_high_grades = df_high_grades.drop(
            ['candidateId', 'questionId', 'Manualscore'], axis=1)

        # Use all under the first 10 rows as Student answers
        df_rest_of_responses = df_high_grades_sorted.iloc[10:]

        for i in range(len(df_rest_of_responses)):

            test = df_rest_of_responses.iloc[[i]]
            manualScore = test.iloc[0]['Manualscore']

            # Remove not relevant attributes for the cosine similarity calculation
            # print(test.candidateId)
            test = test.drop(
                ['candidateId', 'questionId', 'Manualscore'], axis=1)

            # get feedback
            #candidate = test.loc[:, (test != 0).any(axis=0)]
            #candidate_term_list = list(candidate)
            # print(candidate_term_list)
            # feedback_terms = get_feedback_terms(
            #    most_common_terms, candidate_term_list)

            #print("terms for feedback of this candidate " + str(feedback_terms))

            # Calculate the cosine similarity between the response and all other high grades responses
            similarities = cosine_similarity(
                test.values.reshape(1, -1), df_high_grades.values)

            # Calculate the average cosine similarity between one anwer and all high grades responses
            similarities = similarities[0]

            average_similarity = sum(similarities) / len(similarities)

            # Append the manualscore and average cosine similarity to the similarity df
            similarity_df = similarity_df.append(
                {'AVG_cos': average_similarity, 'Manualscore': manualScore}, ignore_index=True)

        # Calculate the pearlson correlation
        correlation = similarity_df['AVG_cos'].corr(
            similarity_df['Manualscore'])
        correlations.append(correlation)
    print(correlations)
    avg_corr = sum(correlations) / len(correlations)
    print(avg_corr)


# reference answer test 3: Half of the highest graded answers
def most_common_terms_halfOfHighestGradedAnswers(year: string):

    path = year + '_tdm_per_question/*.csv'
    correlations = []
    for filename in glob.glob(path):
        df = pd.read_csv(filename)

        similarity_df = pd.DataFrame(columns=['AVG_cos', 'Manualscore'])

        # Instantiate the TruncatedSVD object
        #svd = TruncatedSVD(n_components=100)
        threshold = 0.02

        # Fit the SVD object to the term document matrix
        # svd.fit(df)

        # Create a mask with the same shape as the term document matrix
        mask = np.zeros(df.shape)

        # Set the elements in the mask that correspond to the TF-IDF weights that are greater than or equal to the threshold to 1
        mask[df >= threshold] = 1

        # Multiply the term document matrix with the mask to set the TF-IDF weights under the threshold to 0
        tdm_filtered = df * mask

        # Sort by manualscore
        df_high_grades_sorted = tdm_filtered.sort_values(
            by='Manualscore', ascending=False)

        # Get number of high grade answers
        question_tdm = tdm_filtered.query("Manualscore == 5")
        num_high_grades = len(question_tdm.index)/2

        # Use the first 10 high grade answers as reference answers
        df_high_grades = df_high_grades_sorted.head(int(num_high_grades))

        # Remove not relevant attributes for the cosine similarity calculation
        df_high_grades = df_high_grades.drop(
            ['candidateId', 'questionId', 'Manualscore'], axis=1)

        most_common_terms = extract_common_terms(
            df_high_grades, 20)

        # Use all under the first 10 rows as Student answers
        df_rest_of_responses = df_high_grades_sorted.iloc[int(
            num_high_grades):]

        for i in range(len(df_rest_of_responses)):

            test = df_rest_of_responses.iloc[[i]]
            manualScore = test.iloc[0]['Manualscore']

            # Remove not relevant attributes for the cosine similarity calculation
            test = test.drop(
                ['candidateId', 'questionId', 'Manualscore'], axis=1)

            # get feedback
            candidate = test.loc[:, (test != 0).any(axis=0)]
            candidate_term_list = list(candidate)
            print("most common terms: ", most_common_terms)
            print("candidate term list: ", candidate_term_list)
            feedback_terms = get_feedback_terms(
                most_common_terms, candidate_term_list)

            print("terms for feedback of this candidate " + str(feedback_terms))

            # Calculate the cosine similarity between the response and all other high grades responses
            similarities = cosine_similarity(
                test.values.reshape(1, -1), df_high_grades.values)

            # Calculate the average cosine similarity between one anwer and all high grades responses
            similarities = similarities[0]

            average_similarity = sum(similarities) / len(similarities)

            # Append the manualscore and average cosine similarity to the similarity df
            similarity_df = similarity_df.append(
                {'AVG_cos': average_similarity, 'Manualscore': manualScore}, ignore_index=True)

        # Calculate the pearlson correlation
        correlation = similarity_df['AVG_cos'].corr(
            similarity_df['Manualscore'])
        correlations.append(correlation)
    print(correlations)
    avg_corr = sum(correlations) / len(correlations)
    print(avg_corr)

# Reference answer test 4: Most common words in the corpus
def similarity_against_common_terms(year: string):

    path = year + '_tdm_per_question/*.csv'
    correlations = []
    for filename in glob.glob(path):
        df = pd.read_csv(filename)

        similarity_df = pd.DataFrame(columns=['assignedGrade', 'manualScore'])

        # Sort by manualscore
        df_high_grades_sorted = df.sort_values(
            by='Manualscore', ascending=False)

        # Get number of high grade answers
        question_tdm = df.query("Manualscore == 5")
        num_high_grades = len(question_tdm.index)/2

        # Use the first 10 high grade answers as reference answers
        df_high_grades = df_high_grades_sorted.head(int(num_high_grades))

        # Remove not relevant attributes for the cosine similarity calculation
        df_high_grades = df_high_grades.drop(
            ['candidateId', 'questionId', 'Manualscore'], axis=1)

        # Get the most common terms from the high grades
        most_common_terms = extract_common_terms(df_high_grades_sorted, 100)

        # Use all under the first 10 rows as Student answers
        df_rest_of_responses = df_high_grades_sorted.iloc[int(
            num_high_grades):]

        answer_features_df = pd.read_csv('answer_length_features_2018.csv')

        for i in range(len(df_high_grades_sorted)):

            test = df_high_grades_sorted.iloc[[i]]
            manualScore = test.iloc[0]['Manualscore']

            # Remove not relevant attributes for the cosine similarity calculation
            test = test.drop(
                ['candidateId', 'questionId', 'Manualscore'], axis=1)

            # Find the length of each response from the matrix
            response_length = (test != 0).sum(axis=1).iloc[0]

            length_multiplier = get_length_multiplier(
                response_length, answer_features_df)

            # Define vectors with zeros with the same length as most common terms
            student_answer_vector = np.zeros(len(most_common_terms))
            common_terms_vector = np.zeros(len(most_common_terms))

            for i, term in enumerate(most_common_terms):
                if (term in test.columns):
                    if (test[term] != 0.0).all():
                        student_answer_vector[i] = 1
                    common_terms_vector[i] = 1

            # Calculate the cosine similarity between the response and all other high grades responses
            similarity = cosine_similarity(
                student_answer_vector.reshape(1, -1), common_terms_vector.reshape(1, -1))

            # Calculate the average cosine similarity between one anwer and all high grades responses
            similarity_number = (similarity[0][0] * length_multiplier)
            #grade = cosine_similarity_to_grade(similarity_number)

            # Append the manualscore and average cosine similarity to the similarity df
            similarity_df = similarity_df.append(
                {'assignedGrade': similarity_number, 'manualScore': manualScore}, ignore_index=True)

        # Calculate the number of rows with the same score in both columns
        # num_similar = (similarity_df['assignedGrade']
         #              == similarity_df['manualScore']).sum()

        # Calculate the percentage of rows with the same values
        #percent_similar = num_similar / len(similarity_df) * 100
        #print("Percent similar:", percent_similar)

        # Calculate the pearlson correlation
        correlation = similarity_df['assignedGrade'].corr(
            similarity_df['manualScore'])
        print(correlation)
        correlations.append(correlation)
    print(correlations)
    avg_corr = sum(correlations) / len(correlations)
    print(avg_corr)


    # Run main
if __name__ == "__main__":
    main()
