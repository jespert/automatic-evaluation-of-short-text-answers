from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.feature_extraction.text import TfidfVectorizer
from collections import Counter
from sklearn.metrics import silhouette_score
from sklearn.manifold import TSNE

def main():
    cluster_responses()
    df3 = pd.read_csv('Filer\IT2810Host2018Processed.csv')
    df = df3.query("questionId == 37849173")
    find_optimal_num_clusters(df)

def cluster_responses():
    """
    Function that clusters responses, prints each response in each cluster, plots the purity scores of each cluster,
    plots a stacked bar chart of manualscores in each cluster, plots a TSNE plot and prints the average silhouette score
    
    """
    df3 = pd.read_csv('Filer\IT2810Host2018Processed.csv')

    df = df3.query("questionId == 37849173")

    #Ignore words that appear in less than 10% of the documents
    vectorizer = TfidfVectorizer(min_df= round(0.1 * len(df)), ngram_range=(1, 3))

    # Fit the vectorizer on the column synonyms
    X = vectorizer.fit_transform(df['lemmatized'])

    df2 = pd.DataFrame(X.toarray(), columns=vectorizer.get_feature_names_out())

    # Train the K-Means model
    num_clusters = 9
    kmeans = KMeans(n_clusters=num_clusters)
    kmeans.fit(X)
    labels = kmeans.predict(X)

    results = pd.DataFrame()
    results['candidateId'] = df['candidateId']
    results['questionId'] = df['questionId']
    results['manualscore'] = df['Manualscore']
    results['response'] = df['response']
    results['cluster'] = labels

    print("Top terms per cluster:")
    #Collect the top 10 terms per cluster
    order_centroids = kmeans.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names_out()
    for i in range(num_clusters):
           top_ten_words = [terms[ind] for ind in order_centroids[i, :10]]
           print("Cluster {}: {}".format(i, top_ten_words))
           feedback = ' '.join(top_ten_words)
           print(f'Cluster {i} Common words to: {feedback}')

    #Group the responses using the pandas group by function
    responses_grouped = results.groupby('cluster')
    
    #Loop through all groups
    for label, group in responses_grouped:
        #Print cluster label
        print(f'Cluster {label}:')
        #Iterate over the rows in the pandas group by object
        for i, row in group.iterrows():
            #Print candidateID, QuestionID and response in each cluster
            print(f'Candidate ID: {row["candidateId"]}, Question ID: {row["questionId"]}, Response: {row["response"]}')
        print('\n')

    #Create a dictionary with cluster number as key and all manualscores as values
    cluster_labels = results.groupby('cluster')['manualscore'].apply(list)
    cluster_labels_dict = cluster_labels.to_dict()

    
    #Create a dictionary with cluster numbers as key and first item in values are the most common, and the second value is all labels
    cluster_majority = {}
    for cluster, manualscores in cluster_labels.items():
        majority = max(set(manualscores), key=manualscores.count)
        cluster_majority[cluster] = (majority, manualscores)
    purities = [sum(np.array(labels) == majority) / len(labels) for majority, labels in cluster_majority.values()]
    purity = np.mean(purities)
    print(purity)

    #Make a count dict that counts each manualscore and stores it
    count_dict = {}
    for key, values in cluster_labels_dict.items():
        count_dict[key] = dict(Counter(values))

    # Define the color map for each manualscore
    cmap = {0: 'b', 1: 'g', 2: 'r', 3: 'c', 4: 'm', 5: 'y'}

    # Create a 2d list of all counts for each cluster
    cluster_values = []
    for cluster, counts in count_dict.items():
        # Create a list of counts for each value in the cluster
        value_counts = []
        #Loop through all manualscores
        for value in range(6):
            #Get the count value from count_dict, return 0 if count not found
            value_counts.append(counts.get(value, 0))
        cluster_values.append(value_counts)

    # Convert the 2d list of counts to a numpy array
    cluster_values = np.array(cluster_values, dtype=object)

    # Create the stacked bar chart
    fig, ax = plt.subplots()
    #Loop through all manualscores
    for value in range(max(cmap.keys()) + 1):
        #Set bottom parameter to the sum of last score
        ax.bar(count_dict.keys(), cluster_values[:, value], bottom=cluster_values[:, :value].sum(axis=1), 
            color=cmap[value], width=0.6, label=f'Scored {value}')
    ax.set_xlabel('Cluster ID')
    ax.set_ylabel('Frequency')
    #Set names of each cluster to be shown in the plot
    ax.set_xticks(range(len(count_dict.keys())))
    ax.set_xticklabels(count_dict.keys())
    ax.set_title('Stacked Bar Chart of frequency of each manualscore in each cluster')
    # Put the labels on the right side of the plot
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.subplots_adjust(right=0.8)

    # Show the plot
    plt.show()

    tsne = TSNE(n_components=2, random_state=42)
    X_tsne = tsne.fit_transform(X)

    fig, ax = plt.subplots()
    scatter = plt.scatter(X_tsne[:, 0], X_tsne[:, 1], c=labels, cmap='viridis')
    # produce a legend with the unique colors from the scatter
    legend1 = ax.legend(*scatter.legend_elements(), loc="upper right", title="Clusters")
    #Add the labels to the clusters
    ax.add_artist(legend1)
    plt.show()


    score = silhouette_score(X, labels)
    print("silhouette score:", score)
    
    # for cluster in unique_clusters:
    #    df = results.query("cluster == @cluster")
        #   AVG_grade = df['manualscore'].mean()
        #  print("cluster:", cluster)
        # print("AVG grade:", AVG_grade)


def find_optimal_num_clusters(df):
    """
    Function prints the average silhouette score for each number of cluster up to 12, and finds the
    optimal number of clusters for a question.

    Args:
    df (dataframe): the dataframe to find the optimal number of clusters on.
    """
    silhouette_scores = []
    #Loop through number of clusters
    for n_clusters in range(2, 12):
        #Ignore words that appear in less than 10% of the documents
        vectorizer = TfidfVectorizer(min_df= round(0.1 * len(df)), ngram_range=(1, 3))  

        # Fit the vectorizer on the column synonyms
        X = vectorizer.fit_transform(df['lemmatized'])

        # Train the K-Means model
        kmeans = KMeans(n_clusters=n_clusters)
        kmeans.fit(X)
        labels = kmeans.predict(X)

        silhouette_avg = silhouette_score(X, labels)
        silhouette_scores.append(silhouette_avg)
        print("For n_clusters =", n_clusters, "The average silhouette_score is :", silhouette_avg)

    best_n_clusters = silhouette_scores.index(max(silhouette_scores)) + 2 # add 2 to because starting the loop at 2
    print("Best number of clusters:", best_n_clusters)

    # Run main
if __name__ == "__main__":
    main()