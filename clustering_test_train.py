from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
from sklearn.decomposition import TruncatedSVD
import matplotlib.pyplot as plt
import ast
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.model_selection import train_test_split
from sklearn.metrics import silhouette_score, silhouette_samples
from sklearn.manifold import TSNE
from sklearn.metrics import accuracy_score
from sklearn.metrics import confusion_matrix
import language_tool_python
from sklearn.metrics import classification_report
from collections import Counter
import language_tool_python

def main():
    cluster_responses()
    df3 = pd.read_csv('Filer\IT2810Host2018Processed.csv')

    df = df3.query("questionId == 37849173")
    find_optimal_num_clusters(df)

def cluster_responses():
    """
    Function that clusters responses based on a training set, prints each response in each cluster, plots the purity scores of each cluster, 
    plots a stacked bar chart of manualscores in each cluster, prints feedback to each response and plots a TSNE plot and prints the average silhouette score
    
    """
    df3 = pd.read_csv('Filer\IT2810Host2018Processed.csv')

    df = df3.query("questionId == 37849173")

    #Use manualscore as prediction labels
    y = df['Manualscore']

    #Ignore words that appear in less than 10% of the documents
    vectorizer = TfidfVectorizer(min_df= round(0.1 * len(df)), ngram_range=(1, 3))

    #Split the dataframe into train and test df, set the test size to be 30% of the whole matrix
    train_df, test_df, y_train, y_test = train_test_split(df, y, test_size=0.3, random_state=52)

    # Fit the vectorizer on the column lemmatized in the training set, Only fit the vectorizer on the train data
    X_train = vectorizer.fit_transform(train_df['lemmatized'])


    #Transform the test data into the same numerical representation as the training data, but without refitting the vectorizer.
    #This ensures that the test data is represented in the same way as the training data, but without leaking any information about the test data to the model.
    X_test = vectorizer.transform(test_df['lemmatized'])

    df2 = pd.DataFrame(X_train.toarray(), columns=vectorizer.get_feature_names_out())

    # Insert columns for question id, candidate id and manualscore
    df2.insert(0, "questionId", [x for x in train_df["questionId"].tolist()], True)
    df2.insert(0, "candidateId", [x for x in train_df["candidateId"].tolist()], True)
    df2.insert(2, "Manualscore", [x for x in train_df["Manualscore"].tolist()], True)
    df_high_grades = df2.query("Manualscore == 5") 
    # Remove not relevant attributes for the cosine similarity calculation
    df_high_grades = df_high_grades.drop(['candidateId', 'questionId', 'Manualscore'], axis=1)
    high_grade_terms = extract_common_terms(df_high_grades, 10)
    print(high_grade_terms)

    #LSA dimensionality reduction:
    #svd = TruncatedSVD(n_components=20)
    #X_reduced = svd.fit_transform(X)

    # Train the K-Means model
    num_clusters = 9
    kmeans = KMeans(n_clusters=num_clusters, random_state=52)
    kmeans.fit(X_train, y_train)

    #Predict the cluster/labels of the test set
    test_labels = kmeans.predict(X_test)

    #Check the performance against labels vs predicted labels
    accuracy = accuracy_score(y_test, test_labels)
    print("Accuracy:", accuracy)

    print(classification_report(y_test, test_labels))

    # convert y_test and y_pred to pandas Series objects
    y_test = pd.Series(y_test)
    y_pred = pd.Series(test_labels)

    # concatenate y_test and y_pred side by side using pd.concat()
    side_by_side = pd.concat([y_test.reset_index(drop=True), y_pred.reset_index(drop=True)], axis=1)

    # set column names for the concatenated DataFrame
    side_by_side.columns = ['y_test', 'y_pred']

    # print the concatenated DataFrame
    print(side_by_side)

    #make a resulting dataframe of the test set
    results = pd.DataFrame()
    results['candidateId'] = test_df['candidateId']
    results['questionId'] = test_df['questionId']
    results['manualscore'] = test_df['Manualscore']
    results['lemmatized'] = test_df['lemmatized']
    results['response'] = test_df['response']
    results['cluster'] = test_labels

    print("Top terms per cluster:")
    #Collect the top 10 terms per cluster
    N = 10
    saved_top_N_words = []
    order_centroids = kmeans.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names_out()
    for i in range(num_clusters):
           top_N_words = [terms[term] for term in order_centroids[i, :N]]
           saved_top_N_words.append(top_N_words)
           print("Cluster {}: {}".format(i, top_N_words))
           feedback = ' '.join(top_N_words)

    #Group the responses using the pandas group by function
    responses_grouped = results.groupby('cluster')


    #Loop through all groups
    #for label, group in responses_grouped:
        #Print cluster label
       # print(f'Cluster {label}:')
        #Iterate over the rows in the pandas group by object
       # for i, row in group.iterrows():
            #Print candidateID, QuestionID and response in each cluster
            #print(f'Candidate ID: {row["candidateId"]}, Question ID: {row["questionId"]}, Response: {row["response"]}')
       # print('\n')

    for label, group in responses_grouped:
        print(f'Cluster {label}:')
        for i, row in group.iterrows():
            print(f'Candidate ID: {row["candidateId"]}, Question ID: {row["questionId"]}, , Manualscore: {row["manualscore"]}, response: {row["response"]}, Response lemmatized tokens: {row["lemmatized"]}')
            missing_feedback, including_feedback = get_feedback_terms(saved_top_N_words[label], high_grade_terms)
            missing_in_response, including_in_response = get_feedback_terms(row["lemmatized"], high_grade_terms)
            #print("Cluster is missing:", missing_feedback, "Did you forget to talk about these terms?", "However this cluster mentions important terms like:", including_feedback)
            print("Liknende svar som ditt har skrevet om viktige aspekter som dette:", including_feedback, ", men mangler å skrive om:", missing_feedback,
                   "Dette bør du kanskje skrive mer om:", missing_in_response)
            print('\n')



    all_labels = list(range(num_clusters))
    cluster_labels_dict = {}
    # loop through all possible cluster labels
    for label in all_labels:
        # check if the label is present in the results dataframe
        if label in results['cluster'].unique():
            # if it is, group by the cluster and get the manual scores as a list
            cluster_labels_dict[label] = results.groupby('cluster')['manualscore'].apply(list)[label]
        else:
            # if it is not, add an empty list to the dictionary
            cluster_labels_dict[label] = []

    #Create a dictionary with cluster number as key and all manualscores as values
    cluster_labels = results.groupby('cluster')['manualscore'].apply(list)
    

    
    #Create a dictionary with cluster numbers as key and first item in values are the most common, and the second value is all labels
    cluster_majority = {}
    for cluster, manualscores in cluster_labels.items():
        majority = max(set(manualscores), key=manualscores.count)
        cluster_majority[cluster] = (majority, manualscores)
    purities = [sum(np.array(labels) == majority) / len(labels) for majority, labels in cluster_majority.values()]
    print(purities)
    purity = np.mean(purities)
    print("purity:", purity)

    #Make a count dict that counts each manualscore and stores it
    count_dict = {}
    for key, values in cluster_labels_dict.items():
        count_dict[key] = dict(Counter(values))

    # Define the color map for each manualscore
    cmap = {0: 'b', 1: 'g', 2: 'r', 3: 'c', 4: 'm', 5: 'y'}

    # Create a 2d list of all counts for each cluster
    cluster_values = []
    for cluster, counts in count_dict.items():
        # Create a list of counts for each value in the cluster
        value_counts = []
        #Loop through all manualscores
        for value in range(6):
            #Get the count value from count_dict, return 0 if count not found
            value_counts.append(counts.get(value, 0))
        cluster_values.append(value_counts)

    # Convert the 2d list of counts to a numpy array
    cluster_values = np.array(cluster_values, dtype=object)

    # Create the stacked bar chart
    fig, ax = plt.subplots()
    #Loop through all manualscores
    for value in range(max(cmap.keys()) + 1):
        #Set bottom parameter to the sum of last score
        ax.bar(count_dict.keys(), cluster_values[:, value], bottom=cluster_values[:, :value].sum(axis=1), 
            color=cmap[value], width=0.5, label=f'Scored {value}')
    ax.set_xlabel('Cluster ID')
    ax.set_ylabel('Frequency')
    #Set names of each cluster to be shown in the plot
    ax.set_xticks(range(len(count_dict.keys())))
    ax.set_xticklabels(count_dict.keys())
    ax.set_title('Stacked Bar Chart of frequency of each manualscore in each cluster')
    # Put the labels on the right side of the plot
    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.subplots_adjust(right=0.8)

    # Show the plot
    plt.show()

    score = silhouette_score(X_test, test_labels)
    print("silhouette score:", score)

    sample_silhouette_values = silhouette_samples(X_test, test_labels)

    silhouette_list = []
    for label in range(num_clusters):
        #Find the mean silhouette score for each cluster by finding the mean silhouette for each sample in each cluster
        silhouette_list.append(sample_silhouette_values[test_labels == label].mean())
    print("silhouette scores", silhouette_list)

    
    # for cluster in unique_clusters:
    #    df = results.query("cluster == @cluster")
        #   AVG_grade = df['manualscore'].mean()
        #  print("cluster:", cluster)
        # print("AVG grade:", AVG_grade)



    tsne = TSNE(n_components=2, random_state=52)
    X_test_embedded = tsne.fit_transform(X_test)
    fig, ax = plt.subplots()
    scatter = ax.scatter(X_test_embedded[:, 0], X_test_embedded[:, 1], label=test_labels, c=test_labels, cmap='Paired')
    # produce a legend with the unique colors from the scatter
    legend1 = ax.legend(*scatter.legend_elements(), loc="upper right", title="Clusters", bbox_to_anchor=(1.25, 1))
    #Add the labels to the clusters
    ax.add_artist(legend1)
    # Adjust the position and size of the subplot
    plt.subplots_adjust(right=0.8)

    plt.show()

def extract_common_terms(df, n):
    """
    Function that extractes the N most common terms from high grades
    
    Args:
    df (dataframe): A dataframe that only contains the highly grades answers
    n (int): How many terms to extract
    """
    # Sum up columns in the dataframe
    sum_of_columns = df.sum(axis=0)
    # Return the top n terms in a list
    return sum_of_columns.sort_values(ascending=False).head(n).index.tolist()

def get_feedback_terms(cluster_terms, high_grade_terms):
    """Returns a list of terms from the `cluster_terms` list that are not present in the `ref_answer` list.

    Args:
        cluster_grade_terms (list): A list of terms from cluster.
        ref_answer (list): A list of terms from a reference answer

    Returns:
        list: A list of terms from `cluster_terms` that are not present in `ref_answer`.
        list: A list of terms from `cluster_terms` that are present in `ref_answer`.
    """
    missing_terms = []
    including_terms = []
    df = pd.read_csv('Filer\processed_solutions_2018.csv')
    df['lemmatized'] = df['lemmatized'].apply(ast.literal_eval)
    for term in high_grade_terms:
        if term not in cluster_terms:
            missing_terms.append(term)  
        if term in cluster_terms:
            including_terms.append(term)
    return missing_terms, including_terms


def find_optimal_num_clusters(df):
    """
    Function prints the average silhouette score for each number of cluster up to 12, and finds the
    optimal number of clusters for a question.

    Args:
    df (dataframe): the dataframe to find the optimal number of clusters on.
    """
    silhouette_scores = []
    #Loop through number of clusters
    for n_clusters in range(2, 12):
        #Ignore words that appear in less than 10% of the documents
        vectorizer = TfidfVectorizer(min_df= round(0.1 * len(df)), ngram_range=(1, 3))  
        #Split the dataframe into train and test df, set the test size to be 30% of the whole matrix
        train_df, test_df = train_test_split(df, test_size=0.3, random_state=52)

        # Fit the vectorizer on the column lemmatized in the training set, Only fit the vectorizer on the train data
        X_train = vectorizer.fit_transform(train_df['lemmatized'])

        #Transform the test data into the same numerical representation as the training data, but without refitting the vectorizer.
        #This ensures that the test data is represented in the same way as the training data, but without leaking any information about the test data to the model.
        X_test = vectorizer.transform(test_df['lemmatized'])

        kmeans = KMeans(n_clusters=n_clusters, random_state=52)
        kmeans.fit(X_train)

        #Predict the cluster/labels of the test set
        test_labels = kmeans.predict(X_test)

        silhouette_avg = silhouette_score(X_test, test_labels)
        silhouette_scores.append(silhouette_avg)
        print("For n_clusters =", n_clusters, "The average silhouette_score is :", silhouette_avg)

    best_n_clusters = silhouette_scores.index(max(silhouette_scores)) + 2 # add 2 to because starting the loop at 2
    print("Best number of clusters:", best_n_clusters)

    # Run main
if __name__ == "__main__":
    main()