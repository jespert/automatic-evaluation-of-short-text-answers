from threading import main_thread
import numpy as np
import pandas as pd
import json
import html
import ast
from nltk.corpus import stopwords, wordnet
from nltk.tokenize import RegexpTokenizer
from nltk.stem.snowball import SnowballStemmer
import string
import re
import sys
import html
import phunspell
from langdetect import detect
from polyglot.detect import Detector
import spacy
from spacy.lang.nb.examples import sentences
from sklearn.feature_extraction.text import CountVectorizer

# The language of the snow stemmer
stemmer = SnowballStemmer(language='norwegian')

outDict = {"candidates": []}  # Creates empty dict with list of candidates

# To remove html tags
CLEANR = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')

#Load the norwegian space language model
nlp = spacy.load('nb_core_news_sm')


# Cleans html tags from text
def cleanHTML(text):
    cleantext = re.sub(CLEANR, '', text)
    return cleantext


def main():
    # Add file relative path as command line argument e.g Filer/<filename> if the user wants to run on 2019 dataset
    if len(sys.argv) > 1:
        preprocess_data(sys.argv[1])
        make_dataframe("Filer/IT2810Host2019Processed.json")
    else:
        preprocess_data("Filer/IT2810Host2018.json")
        make_dataframe("Filer/IT2810Host2018Processed.json")
        #preprocess_answers()
        # run_find_synonyms()
        #count_languages("2018")


# Data preprocessing

# Indexing

# Approaches

# Feedback techniques

# Data cleaning

#Load the NLTK stopwords for norwegian
stopwords_nltk = set(stopwords.words('norwegian'))

# Set a list of other norwegian stopwords
other_stopwords = {}
with open('Stopword_list_NO.txt', "r") as f:
    other_stopwords = f.read().split()

other_stopwords = set(other_stopwords)  # Create a set


def make_dataframe(filename: string):
    """
    Function that creates a dataframe from the processed JSON file.
    
    Args:
    filename (string): the name of the file.
    """
    df = pd.json_normalize(outDict["candidates"], record_path="questions", meta=['candidateId'])

    # Run detector on responses and save language name and confidence in columns in the dataframe
    df['language'] = [Detector(x, quiet=True).language.name for x in df['response']]
    df['confidence'] = [Detector(x, quiet=True).language.confidence for x in df['response']]

    # Only keep the responses that are norwegian and english with lower confidence than 85%
    df = df.query("language == 'norsk' | language == 'engelsk' & confidence < 85.0")

    # Define a tokenizer that removes all punctuations
    tokenizer = RegexpTokenizer(r'\w+')
    # Tokenize and transform all letters to lowercase
    df["tokens"] = df["response"].apply(lambda x: [token for token in tokenizer.tokenize(x.lower()) if len(token) > 3 if token.isalpha()])

    # Remove all norwegian stopwords
    df["tokens"] = df["tokens"].apply(remove_stopwords)

    #stemming
    df["stemmed"] = df["tokens"].apply(stem_text)

    #Lemmatize
    df["lemmatized"] = df["tokens"].apply(lemmatize_text)

    #spellcheck
    df["spellChecker"] = df["tokens"].apply(spellcheck_words)

    #Lemmatize + stemming
    df["lemmatizedAndStemmed"] = df["lemmatized"].apply(lambda x: stem_text(x))

    #Lemmatize + synonymlist
    df["lemmatizedAndSynonymList"] = df["lemmatized"].apply(lambda x: find_synonyms(x))

    #Lemmatize + spellcheck
    df["spellCheckAndLemmatized"] = df["spellChecker"].apply(lemmatize_text)

    print("spell check + lemmatized finished")
    print("starting spell check + lemmatized + synonym list")

    #Lemmatize + spellcheck + synonymlist
    df["spellCheckAndLemmatizedAndSynonymList"] = df["spellCheckAndLemmatized"].apply(lambda x: find_synonyms(x))
    df.to_csv(filename[:-5] + ".csv", index=False)


def preprocess_data(filename: string):
    """
    Function that preprocesses the data and creates a JSON file with only the neccessary information.
    
    Args:
    filename (string): the name of the file.
    """
    print("preprocessing " + filename)
    data = pd.read_json(filename)

    for candidate in data['ext_inspera_candidates']:  # For each candidate
        candidateItem = {}  # Create a candidateItem
        # Add candidateID to the candidateitem
        candidateItem["candidateId"] = str(
            candidate['result']['ext_inspera_candidateId'])
        # Add a list of questions to the candidateitem
        candidateItem["questions"] = []
        # for each question in the candidate response
        for question in candidate['result']['ext_inspera_questions']:
            questionItem = {}  # Create a questionItem

            # Add relevant variables to the questionItem
            questionItem["questionId"] = str(
                question['ext_inspera_questionId'])
            questionItem["questiontitle"] = str(
                question['ext_inspera_questionTitle'])
            questionItem["maxQuestionScore"] = str(
                question['ext_inspera_maxQuestionScore'])
            try:  # Check if file has ManualScore attribute
                for score in question['ext_inspera_manualScores']:
                    questionItem["Manualscore"] = str(
                        score['ext_inspera_manualScore'])
            except:
                questionItem["Manualscore"] = "0"
            for response in question['ext_inspera_candidateResponses']:
                questionItem["response"] = str(
                    response['ext_inspera_response'])
                if("<img" in questionItem["response"]):
                    questionItem["response"] = "REMOVE"
                else:
                    questionItem["response"] = html.unescape(
                        questionItem["response"])
                    questionItem["response"] = cleanHTML(
                        questionItem["response"])

            # For each question, append the question to the questions list
            candidateItem["questions"].append(questionItem)

        # For each candidate, append the candidateitem to the candidates list
        outDict["candidates"].append(candidateItem)

    # Delete all question with title React
    for i in range(len(outDict['candidates'])):
        for j in range(len(outDict['candidates'][i]['questions'])):
            if outDict['candidates'][i]['questions'][j]['questiontitle'] == "React":
                del outDict['candidates'][i]['questions'][j]
                break

# Write the whole candidates list to the output file
    with open("Filer/IT2810Host"+filename[-9:-5] + "Processed.json", "w", encoding='utf-8') as outfile:
        json.dump(outDict, outfile,  ensure_ascii=False)
    print("Preprocessing successfull to file IT2810Host" +
          filename[-9:-5] + "Processed.json")


def lemmatize_text(text):
    """
    Function that lemmatizes the text for a column in a dataframe.
    
    Args:
    text (list): the text to lemmatize.
    """
    lemmas = []
    for token in text:
        mylem = nlp(token)
        lemmas.append((" ".join([token.lemma_ for token in mylem])))
    return lemmas


def stem_text(text):
    """
    Function that stemms the text for a column in a dataframe.
    
    Args:
    text (list): the text to stemm.
    """
    return [stemmer.stem(token) for token in text]

def remove_stopwords(tokens):
    """
    Function that only returns the tokens that are not nltk stopwords or other stopwords.
    
    Args:
    tokens (list): the text to check.
    """
    return [token for token in tokens if token not in stopwords_nltk if token not in other_stopwords]


def find_synonyms(text):
    """
    Function that finds norwegian synonyms and changes all synonyms of a word to the base word.
    
    Args:
    text (list): the text to check.
    """
    # Load the norwegian synonyms from JSON
    synonyms = open('norwegian-synonyms.json',  encoding='utf-8')
    textlist = text.copy()
    synonyms_data = json.load(synonyms)
    for i, word in enumerate(text):
        if word in synonyms_data:
            textlist[i] = synonyms_data[word][0]
    return textlist


def find_english_synonyms(text):
    """
    Function that finds english synonyms and changes all synonyms of a word to the base word.
    
    Args:
    text (list): the text to check.
    """
    textlist = []
    for word in text:
        synonyms = []
        # remove unwanted characters
        for syn in wordnet.synsets(word):
            for l in syn.lemmas():
                synonyms.append(l.name())
    return textlist


def spellcheck_words(text):
    """
    Function that corrects words if they are misspelled.
    
    Args:
    text (list): the text to check.
    """
    # Define norwegian and english dictionaries
    en_dictionary = phunspell.Phunspell('en_US')
    no_dictionary = phunspell.Phunspell('nb_NO')
    textlist = []
    for word in text:
        # If word are spelled right in norwegian or english
        if (no_dictionary.lookup(word) or en_dictionary.lookup(word)):
            textlist.append(word)
        # If word are wrong in both english and norwegian
        elif(not no_dictionary.lookup(word) and not en_dictionary.lookup(word)):
            # Check if the spelling error is in norwegian or english
            if(detect(word) == 'no'):
                # Suggest right words
                suggestions = no_dictionary.suggest(word)
                try:
                    # Append first suggestion
                    textlist.append(next(suggestions))
                # If has no suggestion
                except StopIteration:
                    textlist.append(word)
            elif(detect(word) == 'en'):
                # Suggest right words
                suggestions = en_dictionary.suggest(word)
                try:
                    # Append first suggestion
                    textlist.append(next(suggestions))
                # If has no suggestion
                except StopIteration:
                    textlist.append(word)
            # If the error is neither in norwegian or english, do nothing
            else:
                textlist.append(word)
    return textlist



def count_languages(year):
    """
    Function that counts the languages of all responses in the datasets.
    
    Args:
    year (string): the year of the dataset to check.
    """
    df = pd.read_csv('all_responses_' + year + '.csv', index_col=0)
    english = len(df[df['language'] == 'engelsk'])
    english_high_confidence = len(
        df[(df['language'] == 'engelsk') & (df['confidence'] > 85.0)])
    english_low_confidence = len(
        df[(df['language'] == 'engelsk') & (df['confidence'] < 85.0)])
    nynorsk = len(df[df['language'] == 'norsk nynorsk'])
    norwegian = len(df[df['language'] == 'norsk'])
    print("English answers: " + str(english))
    print("English high confidence answers: " + str(english_high_confidence))
    print("English low confidence answers: " + str(english_low_confidence))
    print("Norwegian nynorsk answers: " + str(nynorsk))
    print("Norwegian answers: " + str(norwegian))


def preprocess_answers():
    """
    Function that preprocesses reference answers and adds these to the processed dataframe.
    
    """
    # open the JSON file
    df = pd.read_csv('Filer/IT2810Host2018Processed.csv')
    with open('Filer\suggested_solutions_2018.json', 'r', encoding='utf-8') as file:
        data = json.load(file)

    question_ids = df['questionId'].unique()
    question_ids_list = question_ids.tolist()

    df2 = pd.DataFrame()

    for id in question_ids_list:
        #retrieve the correct solution for that ID
        solution = data["solution_{}".format(id)]
        df2 = add_reference_answer(solution, 2018, id, df2)
    df2.to_csv("processed_solutions_2018.csv")


def add_reference_answer(text, examnumber, questionId, df):
    """
    Function that preprocesses reference answers and adds these to the processed dataframe.
    
    """
    # Define a tokenizer that removes all punctuations
    tokenizer = RegexpTokenizer(r'\w+')
    tokens = tokenizer.tokenize(text)
    text_removed_stopword = remove_stopwords(tokens)
    text_lemmatized = lemmatize_text(text_removed_stopword)
    new_row = {'questionId': questionId, 'questiontitle': "solution{}".format(
        questionId), 'maxQuestionScore': 5, 'Manualscore': 5, 'response': text, 'candidateId': "99999", 'language': "norsk", 'confidence': "99.0", 'tokens': tokens, 'lemmatized': text_lemmatized}
    df2 = df.append(new_row, ignore_index=True)
    return df2


# Run main
if __name__ == "__main__":
    main()
